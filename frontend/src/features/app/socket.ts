import SocketIOClient from 'socket.io-client'
import { WS_HOST } from 'config'

let _socket: SocketIOClientStatic['Socket'] | null
let events: string[] = []

function init() {
  if (_socket) {
    return
  }

  _socket = SocketIOClient(WS_HOST, {
    reconnection: true,
    reconnectionDelay: 500,
    reconnectionAttempts: Infinity,
    transports: ['websocket'],
  })
}

function getSocketInstance() {
  if (_socket) {
    return _socket
  }

  _socket = SocketIOClient(WS_HOST, {
    reconnection: true,
    reconnectionDelay: 500,
    reconnectionAttempts: Infinity,
    transports: ['websocket'],
  })

  return _socket
}

function emit(event: string, data: {[key: string]: string}) {
  const socket = getSocketInstance()

  socket.emit(event, data)
}

function on<T extends Record<string, unknown>>(event: string, callback: (result: T) => void) {
  const socket = getSocketInstance()

  socket.on(event, (data: string) => {
    events.push(event)

    if (!data || !callback) {
      return
    }

    const result = JSON.parse(data)

    callback(result)
  })
}

function off(event: string) {
  const socket = getSocketInstance()

  if (event) {
    events = events.filter((e) => e !== event)
    socket.off(event)

    return
  }

  events.forEach((e) => socket.off(e))
  events = []
}

export const socket = {
  init,
  emit,
  on,
  off,
}