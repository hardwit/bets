import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Bet } from './model'

type IDealsState = {
  bets: Bet[],
  totalCount: number
}

const initialState: IDealsState = {
  bets: [],
  totalCount: 0
}

const betsSlice = createSlice({
  name: 'bets',
  initialState,
  reducers: {
    addBet: (state, action: PayloadAction<Bet>) => {
      state.bets = [action.payload, ...state.bets.slice(0, 99)]
      state.totalCount = state.totalCount + 1
    }
  },
})

const { actions, reducer } = betsSlice

export const { addBet } = actions
export { reducer as betsReducer }