import React, { useEffect } from 'react'
import { socket } from 'features/app/socket'
import { Bet } from './model'
import { useDispatch } from 'react-redux'
import { addBet } from './slice'

type Props = {
  children: JSX.Element
}

export const BetsSocketConnector: React.FC<Props> = ({ children }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    socket.on<Bet>('new_bet', (result) => {
      dispatch(addBet(result))
    })

    return () => socket.off('new_bet')
  }, [])

  return children
}