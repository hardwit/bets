import { useSelector } from "react-redux"
import { Store } from "store/rootReducer"

export const useBets = (): Store['bets'] => {
  const betsState = useSelector((state: Store) => state.bets)

  return betsState
}

