export type Bet = {
  id: number,
  user: string,
  time: string,
  bet: number,
  multiplier: string,
  game: string
}