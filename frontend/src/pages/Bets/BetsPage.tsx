import React, { useState, useCallback } from 'react'
import styled from 'styled-components'
import { BetsSocketConnector, useBets } from 'features/bets'
import { TableContainer, Table, TableRow, TableHead, TableCell, TableBody } from 'ui/atoms/Table'
import { Modal } from 'ui/atoms/Modal'
import { Paper } from 'ui/atoms/Paper'
import { Bet } from 'features/bets/model'


export const BetsPage = () => {
  const { bets, totalCount } = useBets()
  const [betForModal, toggleBetForModal] = useState<Bet | null>(null)

  const handleModalClose = () => {
    toggleBetForModal(null)
  }

  const handleRowClick = useCallback((bet: Bet) => {
    toggleBetForModal(bet)
  }, [])

  return (
    <BetsSocketConnector>
      <PageBox>
        <Header>
          <Title>Bets</Title>
          <Counter count={totalCount} />
        </Header>
        <TableBox>
          <BetsTable 
            bets={bets} 
            onRowClick={handleRowClick}
          />
        </TableBox>

        <BetModal 
          bet={betForModal}
          onClose={handleModalClose}
        />
      </PageBox>
    </BetsSocketConnector>
  )
}

type BetsTableProps = {
  bets: Bet[],
  onRowClick: (bet: Bet) => void
}

const BetsTable: React.FC<BetsTableProps> = ({ bets, onRowClick }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align="right">User</TableCell>
            <TableCell align="right">Time</TableCell>
            <TableCell align="right">Bet</TableCell>
            <TableCell align="right">Multiplier</TableCell>
            <TableCell align="right">Game</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {bets.map((bet) => (
            <BetsTableRow 
              bet={bet}
              key={bet.id}
              onRowClick={onRowClick}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

type RowProps = {
  bet: Bet,
  onRowClick: (bet: Bet) => void
}

const BetsTableRow: React.FC<RowProps> = React.memo(({ bet, onRowClick }) => {
  return (
    <TableRow key={bet.id}>
      <TableCell 
        component="th"
        scope="row"
      >
        <BetId
          onMouseDown={() => onRowClick(bet)}
        >{bet.id}
        </BetId>
      </TableCell>
      <TableCell align="right">{bet.user}</TableCell>
      <TableCell align="right">{bet.time}</TableCell>
      <TableCell align="right">{bet.bet}</TableCell>
      <TableCell align="right">{bet.multiplier}</TableCell>
      <TableCell align="right">{bet.game}</TableCell>
    </TableRow>
  )
})

type CounterProps = {
  count: number
}

const Counter: React.FC<CounterProps> = ({ count }) => {
  return (
    <CounterBox>
      {count}
    </CounterBox>
  )
}

type ModalProps = {
  bet: Bet | null,
  onClose: () => void
}

const BetModal: React.FC<ModalProps> = ({ bet, onClose }) => {
  if (!bet) {
    return null
  }

  return (
    <Modal 
      open
      onClose={onClose}
    >
      <ModalBody>
        Bet ID: {bet.id}
      </ModalBody>
    </Modal>
  )
}

const PageBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Title = styled.span`
  font-size: 40px;
  color: white;
`

const Header = styled.div`
  width: 100%;
  height: 50px;
  padding: 0 40px;
  box-sizing: border-box;
  background-color: #21232f;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const TableBox = styled.div`
  margin-top: 40px;
  padding: 0 40px;
  width: 100%;
  box-sizing: border-box;
`

const BetId = styled.span`
  text-decoration: underline;
  cursor: pointer;
  font-size: 20px;
`

const CounterBox = styled.div`
  font-size: 35px;
  color: white;
`

const ModalBody = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  height: 400px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
  border-radius: 10px;
  font-size: 40px;
  color: #21232f;
  outline: none;
`