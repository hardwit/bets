import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { ROUTES } from 'config/routes'

export const HomePage = () => {
  return (
    <PageBox>
      <Title>Home page</Title>

      <StyledLink to={ROUTES.bets}>Go to bets</StyledLink>
    </PageBox>
  )
}

const PageBox = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Title = styled.span`
  font-size: 40px;
`

const StyledLink = styled(Link)`
  margin-top: 30px;
`