import { combineReducers } from 'redux'
import { betsReducer } from 'features/bets'

export const rootReducer = combineReducers({
  bets: betsReducer
})

export type Store = ReturnType<typeof rootReducer>

export default rootReducer