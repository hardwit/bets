import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { ROUTES } from 'config/routes'
import { HomePage } from 'pages/Home/HomePage'
import { BetsPage } from 'pages/Bets/BetsPage'

export const AppRouter : React.FC = () => {
  return (
    <Switch>
      <Route 
        path={ROUTES.root}
        component={HomePage}
        exact
      />

      <Route path={ROUTES.bets}
        component={BetsPage}
      />
    </Switch>
  )
}