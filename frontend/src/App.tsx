import React from 'react'
import { GlobalStyle } from 'ui/GlobalStyle'
import { AppRouter } from 'routes'

function App() {
  return (
    <>
      <GlobalStyle />
      <AppRouter />
    </>
  )
}

export default App
