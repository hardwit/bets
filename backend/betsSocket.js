let currentId = 1

function initBetsSocket(sockets) {
  setInterval(() => {
    sockets.emit('new_bet', JSON.stringify(generateRandomData()))
  }, 1000 / 30)
}

function generateRandomData() {
  return {
    id: currentId++,
    user: 'Unknown',
    time: getTime(),
    bet: Math.random() * 1000,
    multiplier: '1',
    game: 'Unknown'
  }
}

function getTime() {
  const date = new Date()

  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

module.exports = {
  initBetsSocket
}