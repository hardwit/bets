const { initBetsSocket } = require('./betsSocket')

const app = require('express')()
const http = require('http').createServer(app)
const io = require('socket.io')(http)

http.listen(3001, () => {
  console.log('listening on *:3001')
})

io.on('connection', (socket) => {
})

initBetsSocket(io.sockets)